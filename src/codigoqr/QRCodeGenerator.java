
package codigoqr;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class QRCodeGenerator {

    public static void generateQRCode(String data, String folderPath, String fileName, int width, int height) {
        try {
            BitMatrix bitMatrix = new MultiFormatWriter().encode(data, BarcodeFormat.QR_CODE, width, height);
            
            // Crea un directorio si no existe
            File folder = new File(folderPath);
            if (!folder.exists()) {
                folder.mkdirs();
            }
            
            Path path = FileSystems.getDefault().getPath(folderPath, fileName);
            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);
            
            System.out.println("Código QR generado exitosamente en " + path.toString());
        } catch (WriterException | IOException e) {
            e.printStackTrace();
        }
    }

    
    
// String data = "https://ekuatia.set.gov.py/consultas/qr?nVersion=150&Id=01800276701001001000070412023102317186193241&dFeEmiDE=323032332d31302d32335430393a34333a3535&dRucRec=80105285&dTotGralOpe=5103069.6&dTotIVA=463915&cItems=3&DigestValue=336b675462755146353569376f7679516a325a355670495859756b396f76644c654a6e6c575a5231656b4d3d&IdCSC=0001&cHashQR=b37b20b52ff7847b4a3320b1a681c612b65b3f05d4f2e337aefcbaac43834566";    
    
    
public static void main(String[] args) {
    
    if (args.length == 0) {
        System.out.println("Debes proporcionar un parámetro con el contenido para generar el QR.");        
        
    } 
    else {
        
        String data = args[0]; // El primer argumento es el contenido del QR
        
        String url = data ;
        String idValue = "err";        
        String[] urlParts = url.split("&");        
        
        for (String part : urlParts) {
            if (part.startsWith("Id=")) {        
                idValue = part.substring(3); // 3 es la longitud de "Id="
                break;
            }
        }
                
        
        String folderPath = "imagenqr"; // Nombre de la carpeta donde se guardarán los archivos QR        
        String fileName = idValue+".png"; // Nombre del archivo QR
        int width = 400;
        int height = 400;

        generateQRCode(data, folderPath, fileName, width, height);
    }
}
    
    
}
